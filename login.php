<?PHP

	ini_set("display_errors","On");
	
	require_once "connect.php";

	$username = isset($_POST['username'])?

        filter_input(INPUT_POST,'username',FILTER_SANITIZE_SPECIAL_CHARS) :

        isset($_GET['username']) ?

            filter_input(INPUT_POST,'username',FILTER_SANITIZE_SPECIAL_CHARS)

            : '';

	$password = isset($_POST['password'])?

        filter_input(INPUT_POST,'password',FILTER_SANITIZE_SPECIAL_CHARS) :

        isset($_GET['password']) ?

            filter_input(INPUT_POST,'password',FILTER_SANITIZE_SPECIAL_CHARS)

            : '';
	
	$selection = $con -> prepare("select ID,password from MCUtest.people where ID = :id ");

    $selection->bindValue(':id', $username);

	$selection->execute();

	$result = $selection->fetch();

	$hash = password_hash($result['password'],PASSWORD_DEFAULT);

	if (password_verify($password,$hash))

        header("location:./?error=登入成功");

    else

        echo "帳號錯誤";


?>