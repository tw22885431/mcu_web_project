<?php
	
	$link = array(
		
		'host' => "localhost",
		
		'username' => "root",
		
		'password' => "",
		
		'database' => "MCUTest",
		
	);
	
	/** @var TYPE_NAME $dbconnect */
	$dbconnect = 'mysql:host=' .$link['host'].';database='.$link['database'];
	try {
		
		/** @var TYPE_NAME $con */
		$con = new PDO($dbconnect, $link['username'], $link['password']);

		$con -> query("SET NAMES 'utf8'");
		
		$con -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	}catch (Exception $e){
		
		echo "Connection Failed " .$e->getMessage();
		
		exit();
		
	}
	
?>